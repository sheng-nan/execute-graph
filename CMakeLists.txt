cmake_minimum_required(VERSION 3.16)
project(execute_graph)

set(CMAKE_CXX_STANDARD 14)

add_library(execute_graph
        src/execute_graph.cc src/execute_graph.h
        src/execute_graph.cc src/execute_graph.h
        src/tensor_desc.cc src/tensor_desc.h
        src/shape.cc src/shape.h src/definitions.h src/types.h
        src/tensor.cc src/tensor.h
        src/tensor_data.cc src/tensor_data.h
        src/execute_node.cc src/execute_node.h
        src/op_desc.cc src/op_desc.h
        src/op_def.h
        src/buffer_pool.cc src/buffer_pool.h
        src/edge.cc src/edge.h
        src/operator.cc src/operator.h
        src/execute_graph_builder.cc src/execute_graph_builder.h
        src/execute_node_builder.cc src/execute_node_builder.h
        src/graph_pool_builder.cc src/graph_pool_builder.h
        src/edge_builder.cc src/edge_builder.h
        src/op_def_builder.cc src/op_def_builder.h
        src/attr_utils.cc src/attr_utils.h
        src/any_value.h src/attr_store.cc src/attr_store.h src/small_vector.cc src/small_vector.h src/shape_wrapper.cc src/shape_wrapper.h)

if (NOT ENABLE_OPEN_SRC)
    add_library(test_common
            test/common_reg.cc
            test/common_reg.h
            test/graph_builders.cc
            test/graph_builders.h
            test/ge/infer_shape_functions.cc test/ge/infer_shape_functions.h
            test/ge/infer_shape_utils_by_sn.cc test/ge/infer_shape_utils_by_sn.h
            test/ge/infer_shape_mat_mul.cc test/ge/infer_shape_mat_mul.h
            test/ge/transformer_utils.cc test/ge/transformer_utils.h
            test/stub/transformer.cc test/stub/transformer.h
            )
    target_include_directories(test_common PRIVATE src)
    target_link_libraries(test_common execute_graph)

    add_subdirectory(googletest)
    add_executable(ut
            test/buffer_pool_ut.cc
            test/execute_graph_builder_ut.cc
            test/any_value_ut.cc
            test/attr_store_ut.cc
            test/execute_node_builder_ut.cc
            test/attr_utils_ut.cc test/infer_shape_ut.cc)
    target_link_libraries(ut execute_graph test_common gtest gtest_main)
    target_include_directories(ut PRIVATE src)

    add_subdirectory(google-benchmark)
    add_executable(eg-perf
            test/benchmark_attr.cc
            test/benchmark_tensor_desc.cc
            test/infer_shape_perf.cc)
    target_link_libraries(eg-perf benchmark::benchmark_main benchmark::benchmark execute_graph test_common)
    target_include_directories(eg-perf PRIVATE src)
endif()