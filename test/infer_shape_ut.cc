/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include "graph_builders.h"
#include "common_reg.h"
#include "ge/infer_shape_utils_by_sn.h"


namespace ge {
class InferShapeUt : public testing::Test {};

TEST_F(InferShapeUt, Baseline1InferShape) {
  auto graph = BuildBaselineGraph1();
  auto netoutput = graph->GetNodeByIndex(5);
  EXPECT_NE(netoutput->GetOpDesc()->GetInputDesc(0).GetShape(), Shape({256,256}));

  auto ret = InferShapeUtilsBySn::InferShapeAndDataType(graph.get());

  EXPECT_EQ(ret, SUCCESS);
  EXPECT_EQ(netoutput->GetOpDesc()->GetInputDesc(0).GetShape(), Shape({256,256}));
}
}