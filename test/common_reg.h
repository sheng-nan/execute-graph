/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXECUTE_GRAPH_COMMON_REG_H
#define EXECUTE_GRAPH_COMMON_REG_H
#include "definitions.h"
#include "execute_graph_builder.h"

namespace ge {
struct OpDefRegistry {
  PoolOffset data;
  PoolOffset matmul;
  PoolOffset conv2d;
  PoolOffset netoutput;
  PoolOffset const_offset;
  PoolOffset transdata;
  PoolOffset split;
};
OpDefRegistry RegOpDefs(ExecuteGraphBuilder &gb);

Status TestInferFunc(const Operator &);
Status TestTilingFunc(const Operator &, void *);
}
#endif  //EXECUTE_GRAPH_COMMON_REG_H
