/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <benchmark/benchmark.h>
#include "attr_utils.h"

namespace ge {
#define BENCHMARK_TEST(F)          \
void F(benchmark::State &state); \
BENCHMARK(F);                    \
void F(benchmark::State &state)

struct TestAttrHolder {
  const AttrStore &GetAttrStore() const {
    return store;
  }
  AttrStore &GetAttrStore() {
    return store;
  }

  AttrStore store;
};

BENCHMARK_TEST(SetIntAttrById) {
  TestAttrHolder holder;

  for (auto _ : state) {
    AttrUtils::SetById<TestAttrHolder, int64_t>(holder, 0, 100);
  }
}

BENCHMARK_TEST(GetIntAttrById) {
  TestAttrHolder holder;
  AttrUtils::SetById<TestAttrHolder, int64_t>(holder, 0, 100);

  int64_t value;
  for (auto _ : state) {
    AttrUtils::GetById(holder, 0, value);
  }
}
BENCHMARK_TEST(SetIntAttrByName) {
  TestAttrHolder holder;
  holder.store.SetNameAndId("transpose_x1", 0);
  for (auto _ : state) {
    AttrUtils::Set<TestAttrHolder, int64_t>(holder, "transpose_x1", 100);
  }
}

BENCHMARK_TEST(GetIntAttrByName) {
  TestAttrHolder holder;
  holder.store.SetNameAndId("strides", 0);
  holder.store.SetNameAndId("pads", 1);
  holder.store.SetNameAndId("dilations", 2);
  holder.store.SetNameAndId("groups", 3);
  holder.store.SetNameAndId("data_format", 4);
  holder.store.SetNameAndId("offset_x", 5);
  AttrUtils::SetById<TestAttrHolder, int64_t>(holder, 0, 100);

  int64_t value;
  for (auto _ : state) {
    AttrUtils::Get(holder, "strides", value);
  }
}

BENCHMARK_MAIN();
}