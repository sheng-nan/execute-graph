/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "infer_shape_functions.h"
#include "definitions.h"
#include "operator.h"
#include "execute_node.h"
#include "attr_utils.h"

using namespace ge;
namespace ops {
#define IMPLEMT_COMMON_INFERFUNC(F) \
Status F(const Operator &op)


IMPLEMT_COMMON_INFERFUNC(TransDataInferShape) {
  // main part of shape infer
  auto op_desc = op.GetNode()->GetOpDesc();
  auto &src_tensor = op_desc->GetInputDesc(0);
  auto &src_shape = src_tensor.GetShape();
  DataType input_dtype = src_tensor.GetDataType();
  auto td = op_desc->MutableOutputDesc(0);
  if (src_tensor.GetOriginFormat() == td->GetOriginFormat()) {
    td->SetShape(src_shape);
    td->SetDataType(input_dtype);
  }
  return SUCCESS;
}

IMPLEMT_COMMON_INFERFUNC(DataInferShape) {
  return SUCCESS;
}

IMPLEMT_COMMON_INFERFUNC(DoNothingInferShape) {
  return SUCCESS;
}
}