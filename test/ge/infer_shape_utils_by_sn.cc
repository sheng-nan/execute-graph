/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "infer_shape_utils_by_sn.h"
#include "execute_graph.h"
#include "operator.h"
#include "transformer_utils.h"
namespace ge {
Status InferShapeUtilsBySn::InferShapeAndDataType(ExecuteGraph *graph) {
  NodeShapeTransUtils transformer;
  for (size_t i = 0; i < graph->GetNodeCount(); ++i) {
    auto node = graph->GetNodeByIndex(i);
    auto op_def = node->GetOpDesc()->GetOpDef();
    transformer.SetOpDesc(node->GetOpDesc());
    transformer.Init();
    if (!transformer.CatchFormatAndShape()) {
      return FAILED;
    }
    auto ret = op_def->infer_shape_func(Operator(node));
    if (ret != SUCCESS) {
      return ret;
    }
    if (!transformer.UpdateFormatAndShape())
    return FAILED;
  }
  return SUCCESS;
}
}