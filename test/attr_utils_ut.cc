/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "attr_utils.h"
#include <gtest/gtest.h>

namespace ge {
class AttrUtilsUt : public testing::Test {};

struct TestAttrHolder {
  const AttrStore &GetAttrStore() const {
    return store;
  }
  AttrStore &GetAttrStore() {
    return store;
  }

  AttrStore store;
};

TEST_F(AttrUtilsUt, SetGetOk) {
  TestAttrHolder holder;
  int64_t value;
  auto ret = AttrUtils::SetById<TestAttrHolder, int64_t>(holder, 0, 10);
  EXPECT_TRUE(ret);
  ret = AttrUtils::GetById<TestAttrHolder, int64_t>(holder, 0, value);
  EXPECT_TRUE(ret);
  EXPECT_EQ(value, 10);
}

TEST_F(AttrUtilsUt, GetNotExists) {
  TestAttrHolder holder;
  int64_t value;
  auto ret = AttrUtils::SetById<TestAttrHolder, int64_t>(holder, 0, 10);
  EXPECT_TRUE(ret);
  ret = AttrUtils::GetById<TestAttrHolder, int64_t>(holder, 1, value);
  EXPECT_FALSE(ret);
}

TEST_F(AttrUtilsUt, GetByNameOk) {
  TestAttrHolder holder;
  holder.store.SetNameAndId("transpose_x1", 0);
  holder.store.SetNameAndId("transpose_x2", 1);
  int64_t value;
  auto ret = AttrUtils::SetById<TestAttrHolder, int64_t>(holder, 0, 10);
  EXPECT_TRUE(ret);
  ret = AttrUtils::Get<TestAttrHolder, int64_t>(holder, "transpose_x1", value);
  EXPECT_TRUE(ret);
  EXPECT_EQ(value, 10);
}
}