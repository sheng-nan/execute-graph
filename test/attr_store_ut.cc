/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include "attr_store.h"

namespace ge {
class AttrStoreUt : public testing::Test {};

TEST_F(AttrStoreUt, CreateAndGetOk) {
  AttrStore s;
  EXPECT_TRUE(s.Set<bool>(0, true));
  EXPECT_TRUE(s.Set<bool>(1, false));
  s.SetNameAndId("transpose_x1", 0);
  s.SetNameAndId("transpose_x2", 1);

  bool value = false;
  EXPECT_TRUE(s.Get<bool>(0, value));
  EXPECT_TRUE(value);
  EXPECT_TRUE(s.Get<bool>(1, value));
  EXPECT_FALSE(value);
}

TEST_F(AttrStoreUt, GetNotExists) {
  AttrStore s;
  EXPECT_TRUE(s.Set<bool>(0, true));
  EXPECT_TRUE(s.Set<bool>(1, false));
  s.SetNameAndId("transpose_x1", 0);
  s.SetNameAndId("transpose_x2", 1);

  bool value;
  EXPECT_FALSE(s.Get<bool>(2, value));
  EXPECT_EQ(s.MutableGet<bool>(2), nullptr);
}

TEST_F(AttrStoreUt, ModifyOk) {
  AttrStore s;
  EXPECT_TRUE(s.Set<bool>(0, true));
  EXPECT_TRUE(s.Set<bool>(1, false));
  s.SetNameAndId("transpose_x1", 0);
  s.SetNameAndId("transpose_x2", 1);

  bool value = false;
  EXPECT_TRUE(s.Get<bool>(0, value));
  EXPECT_TRUE(value);
  EXPECT_TRUE(s.Get<bool>(1, value));
  EXPECT_FALSE(value);

  EXPECT_TRUE(s.Set<bool>(0, false));
  value = true;
  EXPECT_TRUE(s.Get<bool>(0, value));
  EXPECT_FALSE(value);
}

TEST_F(AttrStoreUt, ModifyByPointerOk) {
  AttrStore s;
  EXPECT_TRUE(s.Set<bool>(0, true));
  EXPECT_TRUE(s.Set<bool>(1, false));
  s.SetNameAndId("transpose_x1", 0);
  s.SetNameAndId("transpose_x2", 1);

  bool value = false;
  EXPECT_TRUE(s.Get<bool>(0, value));
  EXPECT_TRUE(value);

  auto p = s.MutableGet<bool>(1);
  EXPECT_NE(p, nullptr);
  *p = true;
  value = false;
  EXPECT_TRUE(s.Get<bool>(1, value));
  EXPECT_TRUE(value);
}
}