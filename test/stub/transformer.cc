/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "transformer.h"

namespace ge {
namespace transformer {
inline int64_t CellDiv(int64_t i1, int64_t i2) {
  return (i1 - 1) / i2 + 1;
}
Status ShapeTransferAccordingToFormat::GetShapeAccordingToFormat(ShapeAndFormat &shape_and_format) {
  if (shape_and_format.old_format == FORMAT_NCHW && shape_and_format.new_format == FORMAT_NC1HWC0) {
    auto &old_shape = shape_and_format.old_shape;
    shape_and_format.new_shape.SetDims({old_shape.GetDim(0),
                                  CellDiv(old_shape.GetDim(1), 16),
                                  old_shape.GetDim(2),
                                  old_shape.GetDim(3),
                                  16});
  }

  shape_and_format.new_shape = shape_and_format.old_shape;
  return SUCCESS;
}
bool ExpandDimension(const char *op_type, ge::Format original_format, ge::Format final_format,
                     uint32_t tensor_index, int reshape_type, ge::Shape &dims)
                     {
  return true;
}
}  // namespace transformer
}  // namespace ge