/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXECUTE_GRAPH_TRANSFORMER_H
#define EXECUTE_GRAPH_TRANSFORMER_H
#include "shape.h"
#include "types.h"
namespace ge {
namespace transformer {
struct ShapeAndFormat {
  const Shape &old_shape;
  Shape &new_shape;
  Format old_format;
  Format new_format;
  DataType data_type;
  int64_t impl_type;
};
class ShapeTransferAccordingToFormat {
 public:
  Status GetShapeAccordingToFormat(ShapeAndFormat &shape_and_format);
};
bool ExpandDimension(const char *op_type, ge::Format original_format, ge::Format final_format,
                     uint32_t tensor_index, int reshape_type, ge::Shape &dims);
}
}


#endif  //EXECUTE_GRAPH_TRANSFORMER_H
