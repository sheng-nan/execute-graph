/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "edge_builder.h"
#include "execute_graph_builder.h"
namespace ge {

EdgeBuilder::EdgeBuilder(GraphPoolBuilder *pool_builder, PoolOffset src_node, PoolOffset dst_node, int src_index,
                         int dst_index)
    : pool_builder_(pool_builder), src_node_(src_node), dst_node_(dst_node), src_index_(src_index),
      dst_index_(dst_index) {}
bool EdgeBuilder::operator<(const EdgeBuilder &other) const {
  return std::tie(src_node_, src_index_, dst_node_, dst_index_) <
      std::tie(other.src_node_, other.src_index_, other.dst_node_, other.dst_index_);
}
bool EdgeBuilder::operator==(const EdgeBuilder &other) const {
  return std::tie(src_node_, src_index_, dst_node_, dst_index_) ==
      std::tie(other.src_node_, other.src_index_, other.dst_node_, other.dst_index_);
}
int EdgeBuilder::GetDstIndex() const {
  return dst_index_;
}
PoolOffset EdgeBuilder::GetSrcNode() const {
  return src_node_;
}
int EdgeBuilder::GetSrcIndex() const {
  return src_index_;
}
PoolOffset EdgeBuilder::GetDstNode() const {
  return dst_node_;
}
Status EdgeBuilder::Build(const GraphPoolReader *pool_reader, Edge *edge) const {
  edge->src_node = pool_reader->GetNode(src_node_);
  CHECK_NOT_NULL(edge->src_node);
  edge->src_index = src_index_;

  edge->dst_node = pool_reader->GetNode(dst_node_);
  CHECK_NOT_NULL(edge->dst_node);
  edge->dst_index = dst_index_;

  return SUCCESS;
}
}  // namespace ge
