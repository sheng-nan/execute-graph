/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "attr_store.h"

namespace ge {
AnyValue *AttrStore::GetOrCreate(AttrId attr_id) {
  auto attr_type = GetAttrType(attr_id);
  if (attr_type >= attrs_.size()) {
    return nullptr;
  }
  return &(attrs_[attr_type][GetSubAttrId(attr_id)]);
}
AnyValue *AttrStore::MutableAnyValue(AttrId attr_id) {
  auto attr_type = GetAttrType(attr_id);
  if (attr_type >= attrs_.size()) {
    return nullptr;
  }
  auto &typed_attrs = attrs_[attr_type];
  auto iter = typed_attrs.find(GetSubAttrId(attr_id));
  if (iter == typed_attrs.end()) {
    return nullptr;
  }
  return &iter->second;
}
const AnyValue *AttrStore::GetAnyValue(AttrId attr_id) const {
  auto attr_type = GetAttrType(attr_id);
  if (attr_type >= attrs_.size()) {
    return nullptr;
  }
  auto &typed_attrs = attrs_[attr_type];
  auto iter = typed_attrs.find(GetSubAttrId(attr_id));
  if (iter == typed_attrs.end()) {
    return nullptr;
  }
  return &iter->second;
}
AttrId AttrStore::GetIdByName(const char *name) const {
  auto iter = names_to_id_.find(name);
  if (iter == names_to_id_.end()) {
    return kInvalidAttrId;
  }
  return iter->second;
}
void AttrStore::SetNameAndId(const char *name, AttrId id) {
  names_to_id_[name] = id;
}
}  // namespace ge