/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXECUTE_GRAPH_EDGE_BUILDER_H
#define EXECUTE_GRAPH_EDGE_BUILDER_H
#include "graph_pool_builder.h"
#include "execute_node_builder.h"
#include "buffer_pool.h"
#include "tensor_desc.h"
#include "edge.h"
namespace ge {
class EdgeBuilder {
 public:
  EdgeBuilder(GraphPoolBuilder *pool_builder, PoolOffset src_node, PoolOffset dst_node, int src_index,
              int dst_index);

  bool operator< (const EdgeBuilder &other) const;
  bool operator==(const EdgeBuilder &other) const;

  PoolOffset GetSrcNode() const;
  int GetSrcIndex() const;
  PoolOffset GetDstNode() const;
  int GetDstIndex() const;

  Status Build(const GraphPoolReader *pool_reader, Edge *edge) const;
 private:
  GraphPoolBuilder *pool_builder_;
  PoolOffset src_node_;
  PoolOffset dst_node_;
  int src_index_;
  int dst_index_;
};

}

#endif  //EXECUTE_GRAPH_EDGE_BUILDER_H
