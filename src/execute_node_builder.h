/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXECUTE_GRAPH_EXECUTE_NODE_BUILDER_H
#define EXECUTE_GRAPH_EXECUTE_NODE_BUILDER_H
#include "definitions.h"
#include "execute_node.h"
#include "graph_pool_builder.h"
#include <map>
namespace ge {
class ExecuteGraphBuilder;
class ExecuteNodeBuilder {
 public:
  struct IoDesc {
    PoolOffset name;
    PoolOffset tensor_desc;
  };

  ExecuteNodeBuilder(GraphPoolBuilder *pool_builder, PoolOffset node_offset);

  ExecuteNodeBuilder &SetName(const char *name);
  ExecuteNodeBuilder &SetOpDef(PoolOffset op_def);
  ExecuteNodeBuilder &SetInputName(int index, const char *name);
  ExecuteNodeBuilder &SetOutputName(int index, const char *name);

  // ExecuteGraphBuilder会自动调用的函数
  ExecuteNodeBuilder &SetId(int64_t id);
  ExecuteNodeBuilder &SetInputDesc(int index, PoolOffset td);
  ExecuteNodeBuilder &SetOutputDesc(int index, PoolOffset td);
  ExecuteNodeBuilder &SetAttr(int index, AnyValue &&value);

  PoolOffset GetOffset() const;
  size_t GetInputNum() const;
  size_t GetOutputNum() const;

  Status BuildNode(const GraphPoolReader *pool_reader, ExecuteNode *node);

 private:
  // todo 理论上如果设置了OpDef，而且OpDef中所有输入或所有输出都是Must的，
  //  那么理论上使用者不需要显式指定输入或输出的tensordesc name，Builder也是可以推断出来name的
  //
  Status CheckAndUpdateIoName(const OpDef &op_def);
  Status CheckAndUpdateMustIoName(const IoDef &io_def, int &current_index);
  Status CheckAndUpdateOptionalIoName(const IoDef &io_def, int &current_index);
  Status CheckAndUpdateDynamicIoName(const IoDef &io_def, int &current_index);

 private:
  GraphPoolBuilder *pool_builder_;
  int64_t node_id_{-1};
  PoolOffset node_offset_{kInvalidOffset};
  PoolOffset node_name_{kInvalidOffset};
  PoolOffset op_def_{kInvalidOffset};
  std::map<int, IoDesc> input_indexes_to_desc_;
  std::map<int, IoDesc> output_indexes_to_desc_;
  std::map<int, AnyValue> attr_indexes_to_value_;
  Status error_code_{SUCCESS};
};
}  // namespace ge

#endif  //EXECUTE_GRAPH_EXECUTE_NODE_BUILDER_H
