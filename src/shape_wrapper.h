/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXECUTE_GRAPH_SHAPE_WRAPPER_H
#define EXECUTE_GRAPH_SHAPE_WRAPPER_H
#include <memory>
#include "shape.h"
namespace ge {
class ShapeWrapper {
 public:
  ShapeWrapper();
  ShapeWrapper(std::initializer_list<int64_t> dims);
  ShapeWrapper(const ShapeWrapper &other);
  ShapeWrapper(ShapeWrapper &&other) noexcept;
  ShapeWrapper &operator=(const ShapeWrapper &other);
 private:
  std::shared_ptr<Shape> shape_;
};

}

#endif  //EXECUTE_GRAPH_SHAPE_WRAPPER_H
