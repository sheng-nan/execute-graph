/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "graph_pool_builder.h"
namespace ge {
GraphPoolReader::GraphPoolReader(ExecuteGraph *graph) : graph_(graph) {}
ExecuteNode *GraphPoolReader::GetNode(PoolOffset offset) const {
  if (graph_ == nullptr) {
    return nullptr;
  }
  if (offset >= graph_->node_pool_.size()) {
    return nullptr;
  }
  return &(graph_->node_pool_[offset]);
}
char *GraphPoolReader::GetString(PoolOffset offset) const {
  if (graph_ == nullptr) {
    return nullptr;
  }
  return reinterpret_cast<char *>(graph_->str_pool_.GetBuffer(offset));
}
OpDef *GraphPoolReader::GetOpDef(PoolOffset offset) const {
  if (graph_ == nullptr) {
    return nullptr;
  }
  if (offset >= graph_->op_def_pool_.size()) {
    return nullptr;
  }
  return &(graph_->op_def_pool_[offset]);
}
TensorDesc *GraphPoolReader::GetTensorDesc(PoolOffset offset) const {
  if (graph_ == nullptr) {
    return nullptr;
  }
  if (offset >= graph_->td_pool_.size()) {
    return nullptr;
  }
  return &(graph_->td_pool_[offset]);
}
size_t GraphPoolReader::GetTensorDescCount() const {
  return graph_->td_pool_.size();
}
}
