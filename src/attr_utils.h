/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXECUTE_GRAPH_ATTR_UTILS_H
#define EXECUTE_GRAPH_ATTR_UTILS_H
#include "attr_store.h"
namespace ge {

class AttrUtils {
 public:
  template<class AttrHolder, class ValueType>
  static bool GetById(const AttrHolder &holder, AttrId id, ValueType &value) {
    auto &attr_store = GetAttrStore(holder);
    return attr_store.Get(id, value);
  }
  template<class AttrHolder, class ValueType>
  static bool Get(const AttrHolder &holder, const char *name, ValueType &value) {
    auto &attr_store = GetAttrStore(holder);
    return attr_store.Get(attr_store.GetIdByName(name), value);
  }
  template<class AttrHolder, class ValueType>
  static bool SetById(AttrHolder &holder, AttrId id, const ValueType &value) {
    AttrStore &store = holder.GetAttrStore();
    return store.Set(id, value);
  }
  template<class AttrHolder, class ValueType>
  static bool Set(AttrHolder &holder, const char *name, const ValueType &value) {
    AttrStore &store = holder.GetAttrStore();
    auto id = store.GetIdByName(name);
    if (id == kInvalidAttrId) {
      return false;
    }
    return store.Set(id, value);
  }

  template<class AttrHolder>
  static bool GetBool(const AttrHolder &holder, const char *name, bool &value) {
    return Get(holder, name, value);
  }
  template<class AttrHolder>
  static bool GetBoolById(const AttrHolder &holder, AttrId id, bool &value) {
    return GetById(holder, id, value);
  }
  template<class AttrHolder>
  static bool GetInt64ById(const AttrHolder &holder, AttrId id, int64_t &value) {
    return GetById(holder, id, value);
  }

 private:
  template<class AttrHolder>
  static AttrStore &GetAttrStore(AttrHolder &holder);
  template<class AttrHolder>
  static const AttrStore &GetAttrStore(const AttrHolder &holder);
};
template<typename AttrHolder>
AttrStore &AttrUtils::GetAttrStore(AttrHolder &holder) {
  return holder.GetAttrStore();
}
template<typename AttrHolder>
const AttrStore &AttrUtils::GetAttrStore(const AttrHolder &holder) {
  return holder.GetAttrStore();
}

}  // namespace ge

#endif  //EXECUTE_GRAPH_ATTR_UTILS_H
