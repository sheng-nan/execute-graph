/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXECUTE_GRAPH_DEFINITIONS_H
#define EXECUTE_GRAPH_DEFINITIONS_H
#include <array>
#include <vector>
#include <string.h>
namespace ge {
using Status = int;
constexpr int E_OK = 0;
constexpr int SUCCESS = 0;
constexpr int FAILED = 1;
constexpr int FAILED_NULLPTR = 2;
constexpr int FAILED_EXISTS = 3;

/// limits，超出默认最大值后，使用堆内存
// 默认节点上最大输入或输出的个数
constexpr size_t kDefaultMaxIoCount = 4;
constexpr size_t kDefaultMaxAttrCount = 4;
// 默认所有最大输出个数
constexpr size_t kDefaultMaxDegree = 64;
// 默认最大rank数
constexpr size_t kDefaultRankLimit = 8;
// 默认图上最大节点数量
constexpr size_t kDefaultMaxNodesCount = 8;
// 默认图上最大边数量
constexpr size_t kDefaultMaxEdgesCount = 10;

template<typename T, size_t N>
using SmallVector = std::vector<T>;

#define CHECK_NOT_NULL(ptr) if (ptr == nullptr) return FAILED_NULLPTR;

inline int memcpy_s(void *dst, size_t max, const void *src, size_t len) {
  memcpy(dst, src, len);
  return E_OK;
}

}
#endif  //EXECUTE_GRAPH_DEFINITIONS_H