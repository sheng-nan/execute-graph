/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXECUTE_GRAPH_TYPES_H
#define EXECUTE_GRAPH_TYPES_H
namespace ge {
constexpr int kDataTypeSizeBitOffset = 1000;
enum DataType {
  DT_FLOAT = 0,            // float type
  DT_FLOAT16 = 1,          // fp16 type
  DT_INT8 = 2,             // int8 type
  DT_INT16 = 6,            // int16 type
  DT_UINT16 = 7,           // uint16 type
  DT_UINT8 = 4,            // uint8 type
  DT_INT32 = 3,            //
  DT_INT64 = 9,            // int64 type
  DT_UINT32 = 8,           // unsigned int32
  DT_UINT64 = 10,          // unsigned int64
  DT_BOOL = 12,            // bool type
  DT_DOUBLE = 11,          // double type
  DT_STRING = 13,          // string type
  DT_DUAL_SUB_INT8 = 14,   // dual output int8 type
  DT_DUAL_SUB_UINT8 = 15,  // dual output uint8 type
  DT_COMPLEX64 = 16,       // complex64 type
  DT_COMPLEX128 = 17,      // complex128 type
  DT_QINT8 = 18,           // qint8 type
  DT_QINT16 = 19,          // qint16 type
  DT_QINT32 = 20,          // qint32 type
  DT_QUINT8 = 21,          // quint8 type
  DT_QUINT16 = 22,         // quint16 type
  DT_RESOURCE = 23,        // resource type
  DT_STRING_REF = 24,      // string ref type
  DT_DUAL = 25,            // dual output type
  DT_VARIANT = 26,         // dt_variant type
  DT_BF16 = 27,            // bf16 type
  DT_UNDEFINED = 28,       // Used to indicate a DataType field has not been set.
  DT_INT4 = 29,            // int4 type
  DT_MAX                   // Mark the boundaries of data types
};

inline int GetSizeByDataType(DataType data_type) {
  static int data_type_size[DT_MAX] = {
      4,   // DT_FLOAT = 0,               float type
      2,   // DT_FLOAT16 = 1,             fp16 type
      1,   // DT_INT8 = 2,                int8 type
      4,   // DT_INT32 = 3,
      1,   // DT_UINT8 = 4,               uint8 type
      -1,
      2,   // DT_INT16 = 6,               int16 type
      2,   // DT_UINT16 = 7,              uint16 type
      4,   // DT_UINT32 = 8,              unsigned int32
      8,   // DT_INT64 = 9,               int64 type
      8,   // DT_UINT64 = 10,             unsigned int64
      8,   // DT_DOUBLE = 11,             double type
      1,   // DT_BOOL = 12,               bool type
      -1,  // DT_STRING = 13,             string type
      1,   // DT_DUAL_SUB_INT8 = 14,      dual output int8 type
      1,   // DT_DUAL_SUB_UINT8 = 15,     dual output uint8 type
      8,   // DT_COMPLEX64 = 16,          complex64 type
      16,  // DT_COMPLEX128 = 17,         complex128 type
      1,   // DT_QINT8 = 18,              qint8 type
      2,   // DT_QINT16 = 19,             qint16 type
      4,   // DT_QINT32 = 20,             qint32 type
      1,   // DT_QUINT8 = 21,             quint8 type
      2,   // DT_QUINT16 = 22,            quint16 type
      8,   // DT_RESOURCE = 23,           resource type
      -1,  // DT_STRING_REF = 24,         string ref type
      5,   // DT_DUAL = 25,               dual output type (float + int8)
      8,   // DT_VARIANT                  variant type
      2,   // DT_BF16 = 27,               bf16 type
      -1,  // DT_UNDEFINED = 28           Used to indicate a DataType field has not been set.
      kDataTypeSizeBitOffset + 4,    // DT_INT4 = 29,             int4 type
      // DT_MAX
  };
  if (data_type >= DT_MAX) {
    return -1;
  }
  return data_type_size[data_type];
}

///
/// @brief Calculates the length in bytes based on the DataType and the number of elements.
/// @param element_count
/// @param data_type
/// @return
///
int64_t GetSizeInBytes(int64_t element_count, DataType data_type);

enum Format {
  FORMAT_NCHW = 0,   // NCHW
  FORMAT_NHWC,       // NHWC
  FORMAT_ND,         // Nd Tensor
  FORMAT_NC1HWC0,    // NC1HWC0
  FORMAT_FRACTAL_Z,  // FRACTAL_Z
  FORMAT_NC1C0HWPAD = 5,
  FORMAT_NHWC1C0,
  FORMAT_FSR_NCHW,
  FORMAT_FRACTAL_DECONV,
  FORMAT_C1HWNC0,
  FORMAT_FRACTAL_DECONV_TRANSPOSE = 10,
  FORMAT_FRACTAL_DECONV_SP_STRIDE_TRANS,
  FORMAT_NC1HWC0_C04,    // NC1HWC0, C0 is 4
  FORMAT_FRACTAL_Z_C04,  // FRACZ, C0 is 4
  FORMAT_CHWN,
  FORMAT_FRACTAL_DECONV_SP_STRIDE8_TRANS = 15,
  FORMAT_HWCN,
  FORMAT_NC1KHKWHWC0,  // KH,KW kernel h& kernel w maxpooling max output format
  FORMAT_BN_WEIGHT,
  FORMAT_FILTER_HWCK,  // filter input tensor format
  FORMAT_HASHTABLE_LOOKUP_LOOKUPS = 20,
  FORMAT_HASHTABLE_LOOKUP_KEYS,
  FORMAT_HASHTABLE_LOOKUP_VALUE,
  FORMAT_HASHTABLE_LOOKUP_OUTPUT,
  FORMAT_HASHTABLE_LOOKUP_HITS,
  FORMAT_C1HWNCoC0 = 25,
  FORMAT_MD,
  FORMAT_NDHWC,
  FORMAT_FRACTAL_ZZ,
  FORMAT_FRACTAL_NZ,
  FORMAT_NCDHW = 30,
  FORMAT_DHWCN,  // 3D filter input tensor format
  FORMAT_NDC1HWC0,
  FORMAT_FRACTAL_Z_3D,
  FORMAT_CN,
  FORMAT_NC = 35,
  FORMAT_DHWNC,
  FORMAT_FRACTAL_Z_3D_TRANSPOSE, // 3D filter(transpose) input tensor format
  FORMAT_FRACTAL_ZN_LSTM,
  FORMAT_FRACTAL_Z_G,
  FORMAT_RESERVED = 40,
  FORMAT_ALL,
  FORMAT_NULL,
  FORMAT_ND_RNN_BIAS,
  FORMAT_FRACTAL_ZN_RNN,
  // Add new formats definition here
  FORMAT_END,
  // FORMAT_MAX defines the max value of Format.
  // Any Format should not exceed the value of FORMAT_MAX.
  // ** Attention ** : FORMAT_MAX stands for the SPEC of enum Format and almost SHOULD NOT be used in code.
  //                   If you want to judge the range of Format, you can use FORMAT_END.
  FORMAT_MAX = 0xff
};
}
#endif  //EXECUTE_GRAPH_TYPES_H
