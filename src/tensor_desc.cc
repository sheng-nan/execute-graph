/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "tensor_desc.h"
namespace ge {
Status ge::TensorDesc::SetUnknownDimNumShape() {
  shape_.SetDims({kUnknownDimNum});
  return SUCCESS;
}
void TensorDesc::SetShape(const Shape &shape) {
  shape_ = shape;
}
const Shape &TensorDesc::GetShape() const {
  return shape_;
}
Shape &TensorDesc::MutableShape() {
  return shape_;
}
void TensorDesc::SetOriginShape(const Shape &shape) {
  origin_shape_ = shape;
}
const Shape &TensorDesc::GetOriginShape() const {
  return origin_shape_;
}
Status TensorDesc::SetShapeRange(const Range &range) {
  shape_range_ = range;
  return SUCCESS;
}
Status TensorDesc::GetShapeRange(Range &range) const {
  range = shape_range_;
  return SUCCESS;
}
const Range &TensorDesc::GetShapeRange() const {
  return shape_range_;
}
void TensorDesc::SetFormat(Format format) {
  format_ = format;
}
Format TensorDesc::GetFormat() const {
  return format_;
}
void TensorDesc::SetOriginFormat(Format format) {
  origin_format_ = format;
}
Format TensorDesc::GetOriginFormat() const {
  return origin_format_;
}
void TensorDesc::SetDataType(DataType dt) {
  data_type_ = dt;
}
DataType TensorDesc::GetDataType() const {
  return data_type_;
}
void TensorDesc::SetOriginDataType(DataType dt) {
  origin_data_type_ = dt;
}
DataType TensorDesc::GetOriginDataType() const {
  return origin_data_type_;
}
int64_t TensorDesc::GetId() const {
  return id_;
}
Range &TensorDesc::MutableShapeRange() {
  return shape_range_;
}
const AttrStore &TensorDesc::GetAttrStore() const {
  return attr_store_;
}
AttrStore &TensorDesc::GetAttrStore() {
  return attr_store_;
}
}  // namespace ge
