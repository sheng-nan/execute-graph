/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXECUTE_GRAPH_EXECUTE_GRAPH_H
#define EXECUTE_GRAPH_EXECUTE_GRAPH_H
#include "buffer_pool.h"
#include "execute_node.h"
#include "edge.h"
#include "tensor_desc.h"
#include "definitions.h"
namespace ge {
class ExecuteGraph {
 public:
  bool IsDataEdgeExists(const ExecuteNode *src_node, int src_index, const ExecuteNode *dst_node, int dst_index) const;

  size_t GetNodeCount() const;
  ExecuteNode *GetNodeByIndex(size_t index);
  ExecuteNode *FindNodeByName(const char *name);

  size_t GetEdgeCount() const;
  Edge *GetEdgeByIndex(size_t index);

 private:
  friend class ExecuteGraphBuilder;
  friend class GraphPoolReader;
  // 一旦ExecuteGraph构造完成后，所有的pool都会变成只读
  // 因为这些pool中的内容，已经通过指针被ExecuteGraph的其他数据结构引用了，
  // 如果此时对pool做更改，从而触发了pool的扩容，那可能导致整个pool在内存中换了位置，从而导致所有指针失效
  BufferPool str_pool_;
  std::vector<OpDef> op_def_pool_;
  SmallVector<TensorDesc, kDefaultMaxEdgesCount> td_pool_;
  SmallVector<ExecuteNode, kDefaultMaxNodesCount> node_pool_;
  SmallVector<Edge, kDefaultMaxEdgesCount> edge_pool_;
};
}  // namespace ge

#endif  //EXECUTE_GRAPH_EXECUTE_GRAPH_H
