/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXECUTE_GRAPH_OP_DESC_H
#define EXECUTE_GRAPH_OP_DESC_H
#include "tensor_desc.h"
#include "op_def.h"
#include "definitions.h"
#include "attr_store.h"
namespace ge {
struct IoDesc {
  int index;
  char *name;
  TensorDesc *td;
};
class OpDesc {
 public:
  int64_t GetId() const;
  const char *GetName() const;
  const char *GetType() const;
  const OpDef *GetOpDef() const;

  int GetInputDescCount() const;
  int GetOutputDescCount() const;
  TensorDescPtr MutableInputDesc(int index);
  TensorDescPtr MutableInputDesc(const char *name);
  /// 获取输入TensorDesc，当输入的index不合法（例如越界）或name不存在时，返回`invalid_tensor_desc`，此时可以调用函数
  /// `OpDesc::IsTensorDescExists`来判断返回的TensorDesc是否合法
  const TensorDesc &GetInputDesc(int index) const;
  const TensorDesc &GetInputDesc(const char *name) const;
  TensorDescPtr MutableOutputDesc(int index);
  TensorDescPtr MutableOutputDesc(const char *name);
  const TensorDesc &GetOutputDesc(int index) const;
  const TensorDesc &GetOutputDesc(const char *name) const;

  const char *GetInputNameByIndex(int index) const;
  int GetInputIndexByName(const char *name) const;
  const char *GetOutputNameByIndex(int index) const;
  int GetOutputIndexByName(const char *name) const;

  static bool IsTensorDescExists(const TensorDesc &td);

  bool ValidInputIndex(int index) const {
    return (index >= 0 && index < inputs_.size());
  }
  bool ValidOutputIndex(int index) const {
    return (index >= 0 && index < outputs_.size());
  }
  const AttrStore &GetAttrStore() const;
  AttrStore &GetAttrStore();

 private:
  friend class ExecuteNodeBuilder;
  int64_t id_;  // 唯一标识一个Node，根图内唯一，不会改变。仅有唯一含义，不包含拓扑关系等其他语义
  char *name_;
  OpDef *op_def_;
  SmallVector<IoDesc, kDefaultMaxIoCount> inputs_;
  SmallVector<IoDesc, kDefaultMaxIoCount> outputs_;
  AttrStore attr_store_;
  static TensorDesc invalid_td_;
};
using OpDescPtr = OpDesc *;
}

#endif  //EXECUTE_GRAPH_OP_DESC_H
