/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "execute_node.h"
#include "edge.h"
namespace ge {
ExecuteNode *ExecuteNode::GetInDataNodeByIndex(int index) const {
  auto edge = GetInDataEdgeByIndex(index);
  if (edge == nullptr) {
    return nullptr;
  }
  return edge->src_node;
}
Edge *ExecuteNode::GetInDataEdgeByIndex(int index) const {
  for (auto in_edge : in_data_edges_) {
    if (in_edge->dst_index == index) {
      return in_edge;
    }
  }
  return nullptr;
}
OpDescPtr ExecuteNode::GetOpDesc() {
  return &op_desc_;
}
}

