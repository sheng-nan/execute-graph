/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXECUTE_GRAPH_TENSOR_DESC_H
#define EXECUTE_GRAPH_TENSOR_DESC_H
#include "shape.h"
#include "types.h"
#include "definitions.h"
#include "attr_store.h"
namespace ge {
class TensorDesc {
 public:
  int64_t GetId() const;
  Status SetUnknownDimNumShape();
  void SetShape(const Shape &shape);
  const Shape &GetShape() const;
  Shape &MutableShape();

  void SetOriginShape(const Shape &shape);
  const Shape &GetOriginShape() const;
  Status SetShapeRange(const Range &range);
  // deprecated，这个效率低，是拷贝出来的，不推荐使用
  Status GetShapeRange(Range &range) const;
  const Range &GetShapeRange() const;
  Range &MutableShapeRange();

  void SetFormat(Format format);
  Format GetFormat() const;
  void SetOriginFormat(Format format);
  Format GetOriginFormat() const;

  void SetDataType(DataType dt);
  DataType GetDataType() const;
  void SetOriginDataType(DataType dt);
  DataType GetOriginDataType() const;

  AttrStore &GetAttrStore();
  const AttrStore &GetAttrStore() const;

 private:
  friend class ExecuteGraphBuilder;
  int64_t id_;
  Shape shape_;
  Shape origin_shape_;
  Range shape_range_;
  Range value_range_;  // TODO 考虑做成属性？这东东应该不影响性能，可以不放到TensorDesc里面？
  Format format_;
  Format origin_format_;
  Format storage_format_;
  DataType data_type_;
  DataType origin_data_type_;
  AttrStore attr_store_;
};
using TensorDescPtr = TensorDesc *;
}
#endif  //EXECUTE_GRAPH_TENSOR_DESC_H
