/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "op_desc.h"
#include <cstring>

namespace ge {
TensorDesc OpDesc::invalid_td_;
int64_t OpDesc::GetId() const {
  return id_;
}
const char *OpDesc::GetName() const {
  return name_;
}
const char *OpDesc::GetType() const {
  return op_def_->type;
}
TensorDescPtr OpDesc::MutableInputDesc(int index) {
  if (!ValidInputIndex(index)) {
    return nullptr;
  }
  return inputs_[index].td;
}
TensorDescPtr OpDesc::MutableInputDesc(const char *name) {
  return MutableInputDesc(GetInputIndexByName(name));
}
const TensorDesc &OpDesc::GetInputDesc(int index) const {
  if (!ValidInputIndex(index)) {
    return invalid_td_;
  }
  return *(inputs_[index].td);
}
const TensorDesc &OpDesc::GetInputDesc(const char *name) const {
  return GetInputDesc(GetInputIndexByName(name));
}
bool OpDesc::IsTensorDescExists(const TensorDesc &td) {
  return &td != &invalid_td_;
}
TensorDescPtr OpDesc::MutableOutputDesc(int index) {
  if (!ValidOutputIndex(index)) {
    return nullptr;
  }
  return outputs_[index].td;
}
TensorDescPtr OpDesc::MutableOutputDesc(const char *name) {
  return MutableOutputDesc(GetOutputIndexByName(name));
}
const TensorDesc &OpDesc::GetOutputDesc(int index) const {
  if (!ValidOutputIndex(index)) {
    return invalid_td_;
  }
  return *(outputs_[index].td);
}
const TensorDesc &OpDesc::GetOutputDesc(const char *name) const {
  return GetOutputDesc(GetOutputIndexByName(name));
}
int OpDesc::GetInputIndexByName(const char *name) const {
  // TODO 通过map等手段加速
  for (size_t i = 0; i < inputs_.size(); ++i) {
    if (strcmp(inputs_[i].name, name) == 0) {
      return static_cast<int>(i);
    }
  }
  return -1;
}
int OpDesc::GetOutputIndexByName(const char *name) const {
  for (size_t i = 0; i < outputs_.size(); ++i) {
    if (strcmp(outputs_[i].name, name) == 0) {
      return static_cast<int>(i);
    }
  }
  return -1;
}
const char *OpDesc::GetInputNameByIndex(int index) const {
  if (!ValidInputIndex(index)) {
    return nullptr;
  }
  return inputs_[index].name;
}
const char *OpDesc::GetOutputNameByIndex(int index) const {
  if (!ValidOutputIndex(index)) {
    return nullptr;
  }
  return outputs_[index].name;
}
const OpDef *OpDesc::GetOpDef() const {
  return op_def_;
}
int OpDesc::GetInputDescCount() const {
  return static_cast<int>(inputs_.size());
}
int OpDesc::GetOutputDescCount() const {
  return static_cast<int>(outputs_.size());
}
const AttrStore &OpDesc::GetAttrStore() const {
  return attr_store_;
}
AttrStore &OpDesc::GetAttrStore() {
  return attr_store_;
}
}  // namespace ge