/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXECUTE_GRAPH_EXECUTE_GRAPH_BUILDER_H
#define EXECUTE_GRAPH_EXECUTE_GRAPH_BUILDER_H

#include <map>
#include <set>
#include <string>
#include <list>

#include "graph_pool_builder.h"
#include "execute_graph.h"
#include "execute_node_builder.h"
#include "edge_builder.h"
#include "op_def_builder.h"
namespace ge {
class ExecuteGraphBuilder : public GraphPoolBuilder {
 public:
  ~ExecuteGraphBuilder() override;
  PoolOffset AddString(const char *str) override;
  PoolOffset AddString(const char *str, bool &exists) override;

  PoolOffset AddNode() override;
  PoolOffset AddOpDef(const char *op_type) override;

  // 使用这两个接口需要注意，**不要保存返回的指针**，因为在新增Node、OpDef时，指针可能失效
  ExecuteNodeBuilder *GetNodeBuilder(PoolOffset offset);
  OpDefBuilder *GetOpDefBuilder(PoolOffset offset);


  ExecuteGraphBuilder &AddEdge(PoolOffset src_node, int src_index,
                               PoolOffset dst_node, int dst_index);
  ExecuteGraphBuilder &AddControlEdge(PoolOffset src_node, PoolOffset dst_node);

  std::unique_ptr<ExecuteGraph> Build();


 private:
  Status BuildCheck() const;
  Status BuildPools(ExecuteGraph *graph);
  Status BuildTensorDescPool(ExecuteGraph *graph);
  Status BuildGraphAfterPool(ExecuteGraph *graph);

 private:
  std::map<std::string, PoolOffset> strs_to_offset_;
  BufferPool str_pool_;

  std::map<std::string, PoolOffset> types_to_op_def_offset_;
  std::vector<OpDefBuilder> op_def_pool_;

  std::vector<std::unique_ptr<ExecuteNodeBuilder>> nodes_;

  size_t edge_num_{0};
  std::map<PoolOffset, std::set<EdgeBuilder>> dst_nodes_to_edges_;

  Status error_code_{SUCCESS};
};
}  // namespace ge

#endif  //EXECUTE_GRAPH_EXECUTE_GRAPH_BUILDER_H
