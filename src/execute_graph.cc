/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "execute_graph.h"
#include "edge.h"
namespace ge {
size_t ExecuteGraph::GetNodeCount() const {
  return node_pool_.size();
}
ExecuteNode *ExecuteGraph::GetNodeByIndex(size_t index) {
  if (index >= node_pool_.size()) {
    return nullptr;
  }
  return &node_pool_[index];
}
size_t ExecuteGraph::GetEdgeCount() const {
  return edge_pool_.size();
}
Edge *ExecuteGraph::GetEdgeByIndex(size_t index) {
  if (index >= edge_pool_.size()) {
    return nullptr;
  }
  return &edge_pool_[index];
}
ExecuteNode *ExecuteGraph::FindNodeByName(const char *name) {
  if (name == nullptr) {
    return nullptr;
  }
  for (auto &node : node_pool_) {
    if (strcmp(node.GetOpDesc()->GetName(), name) == 0) {
      return &node;
    }
  }
  return nullptr;
}
}
