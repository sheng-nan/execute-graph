/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXECUTE_GRAPH_OP_DEF_BUILDER_H
#define EXECUTE_GRAPH_OP_DEF_BUILDER_H
#include "buffer_pool.h"
#include "graph_pool_builder.h"
#include "op_def.h"
namespace ge {
class OpDefBuilder {
 public:
  explicit OpDefBuilder(GraphPoolBuilder *pool_builder);
  OpDefBuilder &SetType(const char *type);
  OpDefBuilder &SetInferShapeFunc(InferShapeFunc func);
  OpDefBuilder &SetTilingFunc(TilingFunc func);
  OpDefBuilder &SetInputDefCount(size_t num);
  OpDefBuilder &SetInputDef(int index, const IoDef &def);
  OpDefBuilder &SetOutputDefCount(size_t num);
  OpDefBuilder &SetOutputDef(int index, const IoDef &def);
  OpDefBuilder &SetAttrDefCount(size_t num);
  OpDefBuilder &SetAttrDef(int index, const AttrDef &def);

  Status Build(const GraphPoolReader *pool_reader, OpDef *op_def) const;

 private:
  struct BuilderDef {
    ExistingType e_type{kEndIoType};
    PoolOffset name{kInvalidOffset};
  };
  struct BuilderAttrDef {
    ExistingType e_type{kEndIoType};
    PoolOffset name{kInvalidOffset};
    DefaultAttrValueCreateFunc creator;
  };

 private:
  GraphPoolBuilder *pool_builder_{nullptr};
  PoolOffset type_{kInvalidOffset};
  InferShapeFunc infer_shape_func_{nullptr};
  TilingFunc tiling_func_{nullptr};
  SmallVector<BuilderDef, kDefaultMaxIoCount> inputs_def_;
  SmallVector<BuilderDef, kDefaultMaxIoCount> outputs_def_;
  SmallVector<BuilderAttrDef, kDefaultMaxIoCount> attrs_def_;
  Status error_code_{SUCCESS};
};
}  // namespace ge

#endif  //EXECUTE_GRAPH_OP_DEF_BUILDER_H
