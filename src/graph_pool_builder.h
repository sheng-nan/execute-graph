/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXECUTE_GRAPH_GRAPH_POOL_BUILDER_H
#define EXECUTE_GRAPH_GRAPH_POOL_BUILDER_H
#include <set>
#include <string>
#include "execute_graph.h"
namespace ge {
class GraphPoolBuilder {
 public:
  virtual ~GraphPoolBuilder() = default;

  virtual PoolOffset AddString(const char *str) = 0;
  virtual PoolOffset AddString(const char *str, bool &exists) = 0;

  virtual PoolOffset AddOpDef(const char *op_type) = 0;
  virtual PoolOffset AddNode() = 0;
};

class GraphPoolReader {
 public:
  explicit GraphPoolReader(ExecuteGraph *graph);
  ExecuteNode *GetNode(PoolOffset offset) const;
  char *GetString(PoolOffset offset) const;
  OpDef *GetOpDef(PoolOffset offset) const;
  TensorDesc *GetTensorDesc(PoolOffset offset) const;
  size_t GetTensorDescCount() const;
 private:
  ExecuteGraph *graph_;
};
}  // namespace ge

#endif  //EXECUTE_GRAPH_GRAPH_POOL_BUILDER_H
