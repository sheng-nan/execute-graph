/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "shape_wrapper.h"
#include <memory>
namespace ge {
ShapeWrapper::ShapeWrapper() : shape_(std::make_shared<Shape>()) {}
ShapeWrapper::ShapeWrapper(const ShapeWrapper &other) : shape_(std::make_shared<Shape>(*other.shape_)) {}
ShapeWrapper::ShapeWrapper(ShapeWrapper &&other) noexcept : shape_(std::move(other.shape_)) {}
ShapeWrapper::ShapeWrapper(std::initializer_list<int64_t> dims) : shape_(std::make_shared<Shape>(dims)) {}
ShapeWrapper &ShapeWrapper::operator=(const ShapeWrapper &other) {
  *shape_ = *other.shape_;
  return *this;
}
}  // namespace ge
